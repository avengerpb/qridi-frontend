import React from 'react';
import { render } from 'react-dom';
import Routes from './routes';

import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
import './scss/core/_dropdown-menu-right.scss'
import './scss/style.scss'
import './scss/base.scss';

render(<Routes />, document.getElementById('root'));
