import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

import Login from './views/Login/Login';
import Register from './views/Register';
import App from './app';


const Routes = (props) => {
  return (
    <Router>
      <App>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/register" component={Register} />
        </Switch>
      </App>
    </Router>
  );
};

export default Routes;
